#-------------------------------------------------
#
# Project created by QtCreator 2015-03-11T13:32:05
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = seaBattle
TEMPLATE = app


SOURCES += main.cpp

HEADERS

FORMS

include(Proc/Proc.pri)
include(Client/Client.pri)
include(Settings/Settings.pri)
include(Scanner/Scanner.pri)
include(Struct/Struct.pri)

HEADERS +=

RESOURCES +=
