#include "sender.h"

Sender *Sender::instance = NULL;

Sender::Sender(QObject *parent) : QObject(parent)
{
    udpSocket = new QUdpSocket(this);
    timer = new QTimer(this);

    //connect(timer, SIGNAL(timeout()), this, SLOT(broadcastDatagram()));
}

Sender::~Sender()
{

}

void Sender::broadcastDatagram()
{
    udpSocket->writeDatagram(datagram.data(),datagram.size(),QHostAddress::Broadcast,45454);
}

void Sender::startBroadcasting(QByteArray ba)
{
    datagram = ba;
    broadcastDatagram();
}

Sender* Sender::getInstance()
{
    if (!instance) {
        instance = new Sender();
    }
    return instance;
}
