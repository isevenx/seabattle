#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>

#include <QUdpSocket>

#include "personpool.h"

class Receiver : public QObject
{
    Q_OBJECT
public:
    explicit Receiver(QObject *parent = 0);
    ~Receiver();
    static Receiver *getInstance();
private:
    QUdpSocket *udpSocket;
    static Receiver *instance;
    Receiver(const Receiver &);
    Receiver &operator=(const Receiver &);
signals:
    void drawField(QString name);
    void eraseField(QString player);
    void fullName(QString player);

public slots:
private slots:
    void processPendingDatagrams();
};

#endif // RECEIVER_H
