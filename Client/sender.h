#ifndef SENDER_H
#define SENDER_H

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include <QTimer>

class Sender : public QObject
{
    Q_OBJECT
public:
    explicit Sender(QObject *parent = 0);
    ~Sender();
    void startBroadcasting(QByteArray ba);
    static Sender *getInstance();
private:
    QUdpSocket *udpSocket;
    QTimer *timer;
    QByteArray datagram;
    static Sender *instance;
    Sender(const Sender &);
    Sender &operator=(const Sender &);
public slots:
private slots:
    void broadcastDatagram();

};

#endif // SENDER_H
