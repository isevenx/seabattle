#include "receiver.h"

Receiver *Receiver::instance = NULL;

Receiver::Receiver(QObject *parent) : QObject(parent)
{
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(45454, QUdpSocket::ShareAddress);

    connect(udpSocket, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
}

Receiver::~Receiver()
{

}

void Receiver::processPendingDatagrams()
 {
    QByteArray datagram;
    datagram.resize(udpSocket->pendingDatagramSize());
    udpSocket->readDatagram(datagram.data(), datagram.size());
    QString data = datagram.data();
    if(data.contains("Player:"))
    {
        data.remove(0,7);
        QString tName = data.mid(0,data.indexOf(";"));
        qDebug() << tName;
        QString tIP   = data.mid(data.lastIndexOf(";")+1,data.length()-tName.length());

        PersonPool::getInstance()->addPerson(tName,tIP);
        //PersonPool::getInstance()->printPersons();

        emit drawField(tName);
        emit fullName(data);
    }
    if(data.contains("Closed:"))
    {
        data.remove(0,7);
        emit eraseField(data);
    }
}

Receiver *Receiver::getInstance()
{
    if (!instance) {
        instance = new Receiver();
    }
    return instance;
}
