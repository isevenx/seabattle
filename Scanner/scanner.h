#ifndef SCANNER_H
#define SCANNER_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QTableWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>

namespace Ui {
class Scanner;
}

class Scanner : public QMainWindow
{
    Q_OBJECT

public:
    explicit Scanner(QWidget *parent = 0);
    ~Scanner();

private:
    Ui::Scanner *ui;
    QTableWidget *table;
    void draw();
    QVBoxLayout *mainLayout;
};

#endif // SCANNER_H
