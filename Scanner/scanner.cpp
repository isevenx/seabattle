#include "scanner.h"
#include "ui_scanner.h"

Scanner::Scanner(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Scanner)
{
    ui->setupUi(this);
    this->setWindowTitle("Scanner");
    draw();
}

Scanner::~Scanner()
{
    delete ui;
}

void Scanner::draw()
{
    table = new QTableWidget();
    table->setColumnCount(2);
    table->setHorizontalHeaderLabels(QString("Name;IP").split(";"));
    table->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    table->horizontalHeader()->setStretchLastSection(true);
    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(table);
    ui->centralwidget->setLayout(mainLayout);
}
