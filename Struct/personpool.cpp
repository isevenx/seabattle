#include "personpool.h"

PersonPool *PersonPool::instance = NULL;

PersonPool::PersonPool()
{
}

PersonPool::~PersonPool()
{

}

PersonPool *PersonPool::getInstance() {
    if (!instance) {
        instance = new PersonPool();
    }
    return instance;
}

void PersonPool::addPerson(QString name, QString IP)
{
    persons.append(Person(name,IP));
}

void PersonPool::printPersons()
{
    for(QList<Person>::iterator it = persons.begin(); it< persons.end(); ++it)
    {
        qDebug() << (*it).givePerson().first << (*it).givePerson().second;
    }
}
