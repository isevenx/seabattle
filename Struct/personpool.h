#ifndef PERSONPOOL_H
#define PERSONPOOL_H

#include <QObject>
#include <QDebug>
#include "person.h"

class PersonPool : public QObject
{
    Q_OBJECT
public:
    PersonPool();
    ~PersonPool();
    static PersonPool *getInstance();
    void addPerson(QString name, QString IP);
    void printPersons();
private:
    static PersonPool *instance;
    QList<Person> persons;
signals:

public slots:
};

#endif // PERSONPOOL_H
