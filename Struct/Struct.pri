INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/person.h \
    $$PWD/personpool.h

SOURCES += \
    $$PWD/person.cpp \
    $$PWD/personpool.cpp
