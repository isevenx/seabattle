#ifndef PERSON_H
#define PERSON_H

#include <QObject>
#include <QPair>
#include <QList>

typedef QPair<QString,QString> Pperson;

class Person
{
public:
    Person(QString name,QString ID);
    ~Person();
    Pperson givePerson();
private:
    Pperson person;
};

#endif // PERSON_H
