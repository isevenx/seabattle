#include "settings.h"
#include "ui_settings.h"

Settings *Settings::instance = NULL;

Settings::Settings(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    draw();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::draw()
{

    mainLayout = new QGridLayout;
    save       = new QPushButton("save");

    QLabel *portLabel = new QLabel("set Port");
    port              = new QLineEdit;
    portOk            = new QPushButton("ok");
    QLabel *nameLabel = new QLabel("set name");
    name              = new QLineEdit;
    nameOk            = new QPushButton("ok");

    QHBoxLayout *portLayout = new QHBoxLayout;
    QHBoxLayout *nameLayout = new QHBoxLayout;

    portLayout->addWidget(portLabel);
    portLayout->addWidget(port);
    portLayout->addWidget(portOk);

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(name);
    nameLayout->addWidget(nameOk);

    QGroupBox *portBox = new QGroupBox;
    QGroupBox *nameBox = new QGroupBox;

    portBox->setLayout(portLayout);
    nameBox->setLayout(nameLayout);

    mainLayout->addWidget(portBox,0,1);
    mainLayout->addWidget(nameBox,1,1);
    mainLayout->addWidget(save,2,1);

    ui->centralwidget->setLayout(mainLayout);
}

Settings* Settings::getInstance()
{
    if (!instance) {
        instance = new Settings();
    }
    return instance;
}

QString Settings::getIP()
{
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    QString IP;

    for(int nIter=0; nIter<list.count(); nIter++)
    {
      if(!list[nIter].isLoopback())
          if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol )
        IP = list[nIter].toString();
    }

    if(IP=="")
        IP = "could not detect IP adress";
    return IP;
}
