#ifndef SETTINGS_H
#define SETTINGS_H

#include <QMainWindow>

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>

#include <QPushButton>
#include <QNetworkInterface>
#include <QLineEdit>
#include <QLabel>
#include <QIcon>

#include <QDebug>

namespace Ui {
class Settings;
}

class Settings : public QMainWindow
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();
    static Settings *getInstance();
    QString getIP();
private:
    Ui::Settings *ui;

    QLineEdit *port;
    QPushButton *portOk;
    QLineEdit *name;
    QPushButton *nameOk;
    QPushButton *save;

    QGridLayout *mainLayout;

    static Settings *instance;

    void draw();
};

#endif // SETTINGS_H
