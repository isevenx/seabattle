INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/mainbattle.h \
    $$PWD/fieldview.h \
    $$PWD/field.h \
    $$PWD/directoryanalyzer.h

SOURCES += \
    $$PWD/mainbattle.cpp \
    $$PWD/fieldview.cpp \
    $$PWD/field.cpp \
    $$PWD/directoryanalyzer.cpp

FORMS += \
    $$PWD/mainbattle.ui

