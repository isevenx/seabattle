#ifndef FIELDVIEW_H
#define FIELDVIEW_H

#include <QObject>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QPointF>
#include <QDebug>

class FieldView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit FieldView(QWidget *parent = 0);
    ~FieldView();
signals:
    void sendPoint(QPointF f);
public slots:
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif // FIELDVIEW_H
