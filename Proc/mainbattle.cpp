#include "mainbattle.h"
#include "ui_mainbattle.h"

MainBattle::MainBattle(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainBattle)
{
    ui->setupUi(this);

    DirectoryAnalyzer::getInstance()->useResources();

    sender   = Sender::getInstance();
    receiver = Receiver::getInstance();
    settings = Settings::getInstance();
    scanner  = new Scanner();

    myIP = settings->getIP();

    myPC = "Mihail";
    gBox = new QGridLayout();
    ui->centralWidget->setLayout(gBox);

    sender->startBroadcasting(QString("Player:"+myPC+";IP:"+myIP).toStdString().c_str());

    connect(receiver,SIGNAL(drawField(QString)),this,SLOT(drawField(QString)));
    connect(receiver, SIGNAL(eraseField(QString)),this,SLOT(eraseField(QString)));
    connect(ui->actionSettings,SIGNAL(triggered()),this,SLOT(setSettings()));
    connect(ui->actionConnect,SIGNAL(triggered()),this,SLOT(openScanner()));

}

MainBattle::~MainBattle()
{
    delete ui;
}

void MainBattle::closeEvent(QCloseEvent*)
{
    sender->startBroadcasting(QString("Closed:"+myPC).toStdString().c_str());
}

void MainBattle::drawField(QString name)
{
    if(layouts.size() > 2){
        qDebug() << "Max players";
        return;
    }
    else{
        for(QList<Field*>::iterator it = layouts.begin(); it < layouts.end(); ++it){
            if((*it)->label->text() == name){
                qDebug() << "Clone";
                return;
            }
        }
        if(name != myPC){
            qDebug() << "New player: " << name;
            sender->startBroadcasting(QString("Player:"+myPC).toStdString().c_str());
        }
    }
    Field *field = new Field(this,layouts.size());
    field->label = new QLabel(name);
    field->scene = new QGraphicsScene();
    field->fieldView = new FieldView(field);
    field->fieldView->setScene(field->scene);
    field->fieldView->setMinimumSize(QSize(300,300));
    connect(field->fieldView, SIGNAL(sendPoint(QPointF)),this,SLOT(click(QPointF)));
    layouts.append(field);

    int xI=0,yI=0;
    for (int i=0;i<10;i++)
    {
        for(int j=0;j<10;j++)
        {
            QGraphicsRectItem *item = new QGraphicsRectItem(xI,yI,30,30);
            item->setBrush(QBrush(Qt::gray));
            field->scene->addItem(item);
            field->items.append(item);
            xI+=field->fieldView->width()/10;
        }
        yI +=30;
        xI = 0;
    }

    int x = 0, y = 0;
    for(QList<Field*>::iterator it = layouts.begin(); it < layouts.end(); ++it){

        QVBoxLayout *vBox = new QVBoxLayout();
        vBox->addWidget((*it)->label);
        vBox->addWidget((*it)->fieldView);
        gBox->addLayout(vBox,y,x);
        x++;
        if(x > 2){
            x = 0;
            y++;
        }
    }
}

void MainBattle::eraseField(QString player)
{
    for(QList<Field*>::iterator it = layouts.begin(); it < layouts.end(); ++it){
        if((*it)->label->text() == player){
            (*it)->deleteLater();
            layouts.removeAt(it-layouts.begin());
            break;
        }
    }
}

void MainBattle::setSettings()
{
    settings->show();
}

void MainBattle::click(QPointF f)
{
    double x = 300.0/layouts.at(0)->fieldView->size().width();
    double y = 300.0/layouts.at(0)->fieldView->size().height();
    QPointF point(f.x()*x,f.y()*y);
    for(int i=0;i<layouts.at(0)->items.size();i++)
    {
        if (layouts.at(0)->items.at(i)->contains(point))
        {
            layouts.at(0)->items.at(i)->setBrush(QBrush(Qt::red));
            break;
        }
    }
}

void MainBattle::openScanner()
{
    scanner->show();
}
