#ifndef FIELD_H
#define FIELD_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLabel>

#include "fieldview.h"

class Field : public QWidget
{
    Q_OBJECT
public:
    explicit Field(QWidget *parent = 0, const int &ID = 0);
    ~Field();

    QLabel *label;
    QGraphicsScene *scene;
    FieldView *fieldView;
    QList<QGraphicsRectItem*> items;

    inline int getID() {return ID;}

signals:

public slots:

private:
    int ID;
};

#endif // FIELD_H
