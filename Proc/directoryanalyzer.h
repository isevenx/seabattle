#ifndef DIRECTORYANALYZER_H
#define DIRECTORYANALYZER_H

#include <QObject>
#include <QDirIterator>
#include <QDir>
#include <QFile>
#include <QStringList>

class DirectoryAnalyzer : public QObject
{
    Q_OBJECT
public:
    DirectoryAnalyzer();
    ~DirectoryAnalyzer();
    QString getBuild();
    QString getProject();
    bool makeCopy(QString source, QString destination);
    bool removeDir (const QString & dirName);
    QStringList findFile(QString name,QString path,bool recursive);
    void useResources();
    static DirectoryAnalyzer* getInstance();
private:
    static DirectoryAnalyzer *instance;
signals:

public slots:
};

#endif // DIRECTORYANALYZER_H
