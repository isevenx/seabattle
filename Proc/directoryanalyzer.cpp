#include "directoryanalyzer.h"

DirectoryAnalyzer *DirectoryAnalyzer::instance = NULL;

DirectoryAnalyzer::DirectoryAnalyzer()
{

}

DirectoryAnalyzer::~DirectoryAnalyzer()
{

}

QString DirectoryAnalyzer::getBuild()
{
    QDir dir;
    return dir.absolutePath();
}

QString DirectoryAnalyzer::getProject()
{

    QDir dir;
    QString path = dir.absolutePath();
    path.remove(path.lastIndexOf("/"),path.length()-path.lastIndexOf("/"));
    QStringList nameFilter("main.cpp");
    QDir directory(path);

    QDirIterator it(directory.absolutePath(), nameFilter, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        path =  it.next();
    }
    return path.remove(path.lastIndexOf("/"),path.length()-path.lastIndexOf("/"));
}

QStringList DirectoryAnalyzer::findFile(QString name,QString path,bool recursive)
{
    QStringList list;

    if(recursive)
    {
        QDirIterator it(path, QStringList() << name, QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext())
        {
            list << it.next();
        }        
    }
    else
    {
        QStringList nameFilter(name);
        QDir directory(path);
        list = directory.entryList(nameFilter);
    }
    return list;
}

bool DirectoryAnalyzer::makeCopy(QString source, QString destination)
{
    QDir sourceDir(source);
    if(!sourceDir.exists())
        return false;
    QDir destDir(destination);
    if(!destDir.exists())
    {
        destDir.mkdir(destination);
    }
    QStringList files = sourceDir.entryList(QDir::Files);
    for(int i = 0; i< files.count(); i++)
    {
        QString srcName = source + "/" + files[i];
        QString destName = destination + "/" + files[i];
        QFile::copy(srcName, destName);
    }
    files.clear();
    files = sourceDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for(int i = 0; i< files.count(); i++)
    {
        QString srcName = source + "/" + files[i];
        QString destName = destination + "/" + files[i];
        makeCopy(srcName,destName);
    }
    return true;
}

bool DirectoryAnalyzer::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}

void DirectoryAnalyzer::useResources()
{
    QFile chat(":/images/Images/chat.jpeg");
    if(!QFileInfo(QDir::current().absoluteFilePath("chat.jpeg")).exists()) chat.copy(QDir::current().absoluteFilePath("chat.jpeg"));
    QFile gear(":/images/Images/gear.png");
    if(!QFileInfo(QDir::current().absoluteFilePath("gear.png")).exists()) gear.copy(QDir::current().absoluteFilePath("gear.png"));
}

DirectoryAnalyzer* DirectoryAnalyzer::getInstance()
{
    if (!instance) {
        instance = new DirectoryAnalyzer();
    }
    return instance;
}
