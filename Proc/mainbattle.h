#ifndef MAINBATTLE_H
#define MAINBATTLE_H

#include <QMainWindow>
#include <QThread>
#include <QList>

#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLine>
#include <QPoint>
#include <QLayoutItem>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QFile>
#include <QGraphicsRectItem>

#include "sender.h"
#include "receiver.h"
#include "settings.h"
#include "field.h"
#include "scanner.h"
#include "directoryanalyzer.h"
#include "personpool.h"

namespace Ui {
class MainBattle;
}

class MainBattle : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainBattle(QWidget *parent = 0);
    ~MainBattle();
private:
    Ui::MainBattle *ui;
    QGridLayout *gBox;

    QString myPC;
    QString myIP;

    QList<Field*> layouts;   
    int playerTurn;

    Settings *settings;
    Sender *sender;
    Receiver *receiver;
    Scanner *scanner;

    void closeEvent(QCloseEvent *);

public slots:
    void drawField(QString name);
    void eraseField(QString player);
private slots:
    void setSettings();
    void click(QPointF f);
    void openScanner();
};

#endif // MAINBATTLE_H
