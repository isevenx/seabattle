#include "fieldview.h"

FieldView::FieldView(QWidget *parent) : QGraphicsView(parent)
{
}

FieldView::~FieldView()
{

}

void FieldView::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
          QGraphicsView::mousePressEvent(event);
          QPointF val = event->pos();
          emit sendPoint(val);
    }
}
void FieldView::resizeEvent(QResizeEvent *event)
{
    this->fitInView(this->scene()->sceneRect());
}

